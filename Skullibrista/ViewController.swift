//
//  ViewController.swift
//  Skullibrista
//
//  Created by Gerson Costa on 14/12/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {

    @IBOutlet weak var street: UIImageView!
    @IBOutlet weak var player: UIImageView!
    @IBOutlet weak var viewGameOver: UIView!
    @IBOutlet weak var lbTimePlayed: UILabel!
    @IBOutlet weak var lbInstructions: UILabel!
    
    var isMoving = false
    var gameTimer: Timer!
    var startDate: Date!
    lazy var motionManager = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewGameOver.isHidden = true
        
        street.frame.size.width = view.frame.size.width * 2
        street.frame.size.height = view.frame.size.height * 2
        street.center = view.center
        
        player.center = view.center
        player.animationImages = []
        for i in 0...7 {
            let image = UIImage(named: "player\(i)")!
            player.animationImages?.append(image)
        }
        player.animationDuration = 0.5
        player.startAnimating()
        
        Timer.scheduledTimer(withTimeInterval: 6.0, repeats: false) { (timer) in
            self.start()
        }
    }
    
    func start() {
        lbInstructions.isHidden = true
        viewGameOver.isHidden = true
        isMoving = false
        startDate = Date()
        
        player.transform = CGAffineTransform(rotationAngle: 0)
        street.transform = CGAffineTransform(rotationAngle: 0)
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.startDeviceMotionUpdates(to: OperationQueue.main, withHandler: { (data, error) in
                if error == nil {
                    if let data = data {
                        let angle = atan2(data.gravity.x, data.gravity.y) - .pi
                        self.player.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
                        if !self.isMoving {
                            self.checkGameOver()
                        }
                    }
                }
            })
        }
        gameTimer = Timer.scheduledTimer(withTimeInterval: 4, repeats: true, block: { (time) in
            self.rotateWorld()
        })
    }
    
    func rotateWorld() {
        let randomAngle = Double(arc4random_uniform(120)) / 100 - 0.6
        isMoving = true
        
        UIView.animate(withDuration: 0.75, animations: {
            self.street.transform = CGAffineTransform(rotationAngle: CGFloat(randomAngle))
        }) { (success) in
            self.isMoving = false
        }
    }
    
    func checkGameOver() {
        let worldAngle = atan2(Double(street.transform.a), Double(street.transform.b))
        let playerAngle = atan2(Double(player.transform.a), Double(player.transform.b))
        let difference = abs(worldAngle - playerAngle)
        print(difference)
        if difference > 0.25 {
            gameTimer.invalidate()
            viewGameOver.isHidden = false
            motionManager.stopDeviceMotionUpdates()
            let secondsPlayed = round(Date().timeIntervalSince(startDate))
            lbTimePlayed.text = "Voce jogou durante \(secondsPlayed) segundos"
        }
    }

    @IBAction func playAgain(_ sender: UIButton) {
        start()
    }
    
}

